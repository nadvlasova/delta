from datetime import date
from views import Index, About, Courses, Simulator


# front controller
def secret_front(request):
    request['data'] = date.today()


def other_front(request):
    request['key'] = 'key'


fronts = [secret_front, other_front]

routes = {
    '/index/': Index(),
    '/about/': About(),
    '/courses/': Courses(),
    '/simulator/': Simulator(),
}

