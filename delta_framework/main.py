"""Класс Framework - основа фреймворка"""
import quopri
from os import path


class PageNotFound404:
    def __call__(self, request):
        return '404 WHAT', '404 PAGE Not Found'


class Framework:

    def __init__(self, routes_obj, fronts_obj):
        self.routes_lst = routes_obj
        self.fronts_lst = fronts_obj

    def __call__(self, environ, start_response):
        # получаем адрес, по которому выполнен переход
        path = environ['PATH_INFO']

        # добавление закрывающего слеша
        if not path.endswith('/'):
            path = f'{path}/'

        # находим нужный контроллер
        # отработка паттерна page controller
        if path in self.routes_lst:
            view = self.routes_lst[path]
            # content_type = self.get_content_type(path)

        else:
            view = PageNotFound404()
        request = {}
        # наполняем словарь request элементами
        # этот словарь получат все контроллеры
        # отработка паттерна front controller
        for front in self.fronts_lst:
            front(request)
        # запуск контроллера с передачей объекта request
        code, body = view(request)
        start_response(code, [('Content-Type', 'text/html')])
        return [body.encode('utf-8')]

    # @staticmethod
    # def get_content_type(file_path, content_types_map=CONTENT_TYPES_MAP):
    #     file_name = path.basename(file_path).lower()  # styles.css
    #     extension = path.splitext(file_name)[1]  # .css
    #     print(extension)
    #     return content_types_map.get(extension, "text/html")
    #
    # @staticmethod
    # def get_static(static_dir, file_path):
    #     path_to_file = path.join(static_dir, file_path)
    #     with open(path_to_file, 'rb') as f:
    #         file_content = f.read()
    #     status_code = '200 OK'
    #     return status_code, file_content
    #
    # @staticmethod
    # def decode_value(data):
    #     new_data = {}
    #     for k, v in data.items():
    #         val = bytes(v.replace('%', '=').replace("+", " "), 'UTF-8')
    #         val_decode_str = quopri.decodestring(val).decode('UTF-8')
    #         new_data[k] = val_decode_str
    #     return new_data
