from delta_framework.templator import render


class Index:
    def __call__(self, request):
        return '200 OK', render('index.html', data=request.get('data', None))


class About:
    def __call__(self, request):
        return '200 OK', render('about.html', data=request.get('data', None))


class Courses:
    def __call__(self, request):
        return '200 OK', render('courses.html', data=request.get('data', None))


class Simulator:
    def __call__(self, request):
        return '200 OK', render('simulator.html',
                                data=request.get('data', None))


class NotFound404:
    def __call__(self, request):
        return '404 WHAT', '404 Page Not Found'
